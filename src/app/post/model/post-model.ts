export class Post {
  id: number
  author: string
  postTime: Date
  readTimes: number
  commentTimes: number
  title: string
  text: string
}